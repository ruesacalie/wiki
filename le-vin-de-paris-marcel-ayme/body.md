# Le Vin de Paris

> Le Vin de Paris est un recueil de nouvelles de Marcel Aymé, publié en 1947.  Les huit récits qui le composent ont pour point commun de se dérouler à Paris pendant (ou juste après) l’Occupation allemande. Le premier d'entre eux, L’Indifférent, s'inspire librement de l'affaire Petiot. Les thèmes abordés sont liés aux préoccupations de cette époque : la pénurie, la lâcheté, la collaboration etc.  Trois de ces nouvelles, La Grâce, La Bonne Peinture et la Traversée de Paris, ont été adaptées pour la télévision et le cinéma.
