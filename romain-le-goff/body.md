# Romain Le Goff

## Hommage paru dans le quotidien _Le XIXe siècle_ du 10 octobre 1886

CHRONIQUE

C'est une des traditions de ce journal que de prendre en main la cause des bons serviteurs du pays, oubliés ou envers lesquels il y a quelque injustice à réparer. Je demande permission de signaler aujourd'hui une grande infortune imméritée, et je veux croire qu'il suffira de ces poignantes révélations pour que la situation, quasi désespérée, d'une femme de cœur qui a droit à de grands égards, se modifie. Grâce au ciel, dans notre pays on ne reste guère indifférent lorsqu'il s'agit d'une patriotique réparation à accomplir.

Il y a, dans le quartier Saint-Jacques, une rue qui porte aujourd'hui le nom de rue « Le Goff ». Les faits qu'elle rappelle ont pu sortir passagèrement de la mémoire; mais ils ne sont pas, assurément, complètement effacés de l'esprit.

Il y avait, en 1879, à l'hôpital militaire du Val-de-Grâce un jeune chirurgien très épris de science, très passionné pour les théories les plus audacieuses de la médecine nouvelle. Un jour, entra dans son service — au lit 16 de la salle 29 — un soldat nommé Loiseau, qui se mourait d'anémie. Ce soldat avait fait la campagne de 1871 dans l'Est, et il ne s'était jamais remis des fatigues qu'il avait eu à supporter. Il avait traîné pendant huit ans, puis l'anémie était arrivée à son dernier période. Son état était désespéré.

C'est alors que Romain Le Goff, le jeune chirurgien dont je parle, offrit aux médecins prinaipaux de l'hôpital de se prêter à une tentative qui n'avait alors été qu'assez rarement faite en France. Il s'agissait de l'opération de la transfusion du sang. Cette opération paraissait, avec raison, chose grave. Le docteur Goujon hésitait à la permettre ; Romain Le Goff alla trouver le professeur Pingaud, et, à force d'insistance, le décida, dans l'intérêt de la science, à accomplir cette épreuve.

Le Goff, très crânement, se dévêtit, laissa appliquer sur lui, après qu'on eut pratiqué sur son bras une large saignée, l'appareil qui allait porter dans les veines du moribond son sang généreux. Il avait vingt-cinq ans ; il était dans toute la force et toute la vaillance d'un tempérament solide. Il ne broncha pas une seconde pendant l'opération, et, tant qu'elle dura, il plaisanta, ranimant le courage du soldat, très étonné et très troublé d'un dévouement qui devait le sauver, lui. Et de fait, il guérit.

Mais Le Goff ne se remit pas, en dépit de sa santé et de sa vigueur. Il rentra chez lui encore souriant, affirmant qu'il n'avait fait là que la chose la plus simple du monde. Dans la nuit, il était pris d'une fièvre violente, puis il tombait dans une inquiétante prostration, et quand, après deux mois de lit, il se relevait, il était affreusement changé.

Je le rencontrai, à cette époque. Le Goff avait été un de mes bons camarades, au lycée Saint-Louis, où nous avions fait nos études côte à côte. Je ne pus réprimer, en l'apercevant, un geste de surprise douloureuse. Je me le rappelais robuste, trapu, ayant parmi ses condisciples une réputation de gymnaste, habile à tous les exercices de corps, et ayant comme passe-temps favori des exercices de force où il s'amusait à soulever des poids formidables.

En ce temps-là, le carrefour de l'Observatoire donnait asile, tous les jeudis, à un athlète en plein vent qui enlevait un homme à cheval sur un tonneau, rien qu'avec ses dents. Le Goff s'entraînait à l'imiter. Il était petit, bien pris, très brun. Je ne pouvais me figurer Le Goff qu'accomplissant quelque prouesse physique. Et il était là, blême, se traînant à peine, emmitouflé d'un cache-nez blanc, s'appuyant sur une canne.

Il était médecin, il ne se faisait pas d'illusion sur son état : « Je suis fichu », me dit-il simplement. Un mois après, il partait pour l'Algérie, où on l'avait nommé maître de conférences à l'Ecole de médecine d'Alger. Quelques semaines se passaient; puis, tout à coup, par un mot de son père, qui avait été secrétaire général des postes et télégraphes pendant la guerre, j'apprenais sa mort.

On doit se souvenir combien Paris s'émut de cette mort, qui avait été causée par l'inspiration la plus généreuse qui fut. Les journaux racontèrent longuement l'acte héroïque de Le Goff, qu'il avait accompli avec tant de modestie. A Alger, on fit au jeune savant de belles funérailles. - Le conseil municipal de Paris, enfin, débaptisa une rue - longeant l'institution des sourds - muets, pour lui donner, répondant ainsi au sentiment unanime, le nom de ce charmant, bon et courageux Le Goff.

Le malheur était entré dans cette famille. Trois ans après, M. Le Goff père mourait, laissant sa femme dans une situation précaire.

Cette digne femme se trouve aujourd'hui sans aucuns moyens d'existence.

Cette mère, qui ne cesse de pleurer le fils qui lui a été enlevé, est, pour dire nettement les choses, dans la misère.

Elle a la pudeur de son malheur; elle ne veut pas, pour solliciter un - emploi qui la tirerait du dénuement, invoquer le souvenir respecté de Romain Le Goff.

C'est par hasard que j'ai appris la détresse de la mère de mon ancien camarade, et je prends sur moi de la dire publiquement. Il ne me paraît pas possible que l'Etat refuse à cette digne personne, si cruellement éprouvée, privée, pour une cause si honorable, de son soutien naturel, quelque position modeste qui assure le repos de sa vieillesse. A qui l'Etat doit-il son aide, si ce n'est à la famille de ceux qui se sont sacrifiés dans un but d'intérêt général ?

Ce serait à désespérer de tout si, avec de tels titres, Mme Le Goff devait, faute de protections, n'attendre plus rien que du hasard.

C'est fort bien, sans doute, que de perpétuer la mémoire des braves gens en donnant leur nom à une rue ; mais la meilleure marque de piété envers le souvenir de Romain Le Goff, n'est-ce pas d'assurer la vie de sa mère ?

PAUL GINISTY.

Source : Bibliothèque nationale de France - Gallica - https://gallica.bnf.fr/ark:/12148/bpt6k7560019c/

## Liens

* <https://francearchives.fr/facomponent/09800c1f8964a93cea5e1ad33c22999b0f82b18c#>
