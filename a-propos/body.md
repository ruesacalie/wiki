# À propos

::: script
types =  sam.getInstances('.type')
:::

::: div has-text-centered
[![le Dit des rues de Paris](./le-dit-des-rues-de-paris/media/le-dit-incipit.jpg "Incipit du Dit des rues de Paris, Guillot de Paris, 1300")](le-dit-des-rues-de-paris) 
:::

_Rue Sacalie_ est un wiki sur l'histoire des rues de Paris, sur des personnages les ayant traversées et sur des œuvres y faisant référence. Mentionnée dès la première strophe du _[Dit des rues de Paris](le-dit-des-rues-de-paris)_ (1300) juste après la [rue de la Huchette](rue-de-la-huchette) dans le quartier d'« outre-petit-pont », la rue Sacalie est aujourd'hui la [rue Xavier-Privas](rue-xavier-privas), dans le [Quartier latin](quartier-latin).


Parcourir le wiki : {{~it.types :value:index}}{{? ![".aek", ".typ"].includes(value.reference.id) }}[{{=value.label.toLowerCase()}}s]({{=value.reference.id}}){{? ((it.types.length-1)>index)}},{{?}} {{?}}{{~}}

::: figure
[![](le-dit-des-rues-de-paris/media/le-dit-preface-1875-plan-petit.jpg "Fragment d'un plan de Paris issu du Dit des rues de Paris")](le-dit-des-rues-de-paris)

_Plan des rues de Paris issu du [Dit des rues de Paris](le-dit-des-rues-de-paris)_
:::

::: figure
[![](vue-de-paris-1650-depuis-le-faubourg-saint-jacques/media/vue-1.jpg)](vue-de-paris-1650-depuis-le-faubourg-saint-jacques)

_Vue de Paris, vers 1650, depuis le faubourg Saint-Jacques – [Musée Carnavalet](musee-carnavalet)_
:::

::: div has-text-centered
[Fil des modifications](.history)

 [@ruesacalie](https://twitter.com/ruesacalie)
 :::
