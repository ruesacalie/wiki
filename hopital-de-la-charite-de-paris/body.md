# Hôpital de la Charité de Paris

{{quote}}
DEUIL

On annonce la mort d'Alfred Jarry, un des esprits les plus curieux de la jeune littérature, qui connut même une heure de gloire avec la création de ce type inoubliable : Ubu Roi.

Jarry est mort à l'hôpital de la Charité, où il venait d'être transporté, grâce aux bons soins du docteur Saltas et de M. Alfred Vallette.

Il était âgé de trente-trois ans.

Source : BnF Gallica https://gallica.bnf.fr/ark:/12148/bpt6k7625486v/f2.item.zoom.texteImage
{{/quote}}