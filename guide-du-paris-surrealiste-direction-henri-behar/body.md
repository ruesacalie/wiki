# Guide du Paris surréaliste - Direction Henri Béhar


* http://www.editions-du-patrimoine.fr/Librairie/Guides/Guide-du-Paris-surrealiste

## Sommaire

* Le Paris des surréalistes, par Henri Béhar
* Équivoque, moderne et merveilleux : le Paris de Louis Aragon, par Mireille Hilsum
* Les pas perdus d'André Breton, par Emmanuel Rubio
* René Crevel, archange du surréalisme, par Jean-Michel Devésa
* Le Paris gouailleur, sanglant et mystérieux de Robert Desnos, par Laurent Flieder
* Le Paris réel et surréel de Jacques Prévert, par Danièle Gasiglia-Laster
* Philippe Soupault, flâneur entre deux rives, par Myriam Boucharenc
* Lieux surréalistes, par Henri Béhar
* Bibliographie
