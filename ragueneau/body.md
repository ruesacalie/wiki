# Ragueneau

- http://www.cyranodebergerac.fr/roxane_autres_contenu.php?contenu_id=1
- https://caferagueneau.com/

> Hasard du destin, le café restaurant Ragueneau se trouve à l’emplacement d’un ancien théâtre où Molière joua ses premières pièces !  [Source](https://caferagueneau.com/lame-du-lieu/)
