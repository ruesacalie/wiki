# Guide de Paris Mystérieux

> La légende, les énigmes, les mythes, les lieux sacrés, les pierres étranges, les souterrains et bien d'autres choses sur la capitale de la France.
"Paris a sa faune et sa flore qu'ignore le voyageur. Paris a son histoire, présente à chaque pas, Paris est à l'image du temps. Pour les auteurs de ce guide, Paris est une légende." Sous la direction de François Caradec, les auteurs de ce guide nous invitent à flâner à travers toutes les rues de la capitale en nous signalant les événements insolites, historiques ou récents qui s'y sont déroulés.
