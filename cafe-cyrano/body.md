# Café Cyrano

## Liens

* https://www.paris-bistro.com/univers/cafes-et-histoire/les-cafes-des-surrealistes
* https://terresdecrivains.com/Le-cafe-Cyrano
* https://www.parisrevolutionnaire.com/spip.php?article1059
* http://memoire-et-societe.over-blog.com/article-au-berceau-du-surrealisme-110168501.html

## Sources

* Source de la photo : https://terresdecrivains.com/Le-cafe-Cyrano
