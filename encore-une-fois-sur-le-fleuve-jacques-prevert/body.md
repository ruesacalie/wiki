# Encore une fois sur le fleuve - Jacques Prévert

Source : _Histoires_, Jacques Prévert

\[à vérifier\]

Encore une fois sur le fleuve
le remorqueur  de l'aube
a  poussé son cri

Et  encore une  fois
le soleil  se  lève
le  soleil  libre  et  vagabond
qui  aime à dormir  au  bord des rivières
sur la  pierre
sous  les  ponts
Et  comme la nuit  au  doux  visage  de  lune
tente de  s'esquiver
furtivement
le prodigieux clochard au  réveil  triomphant
le grand  soleil paillard  bon  enfant  et  souriant
plonge  sa grande main chaude dans le  décolleté de la  nuit
et d'un coup  lui  arrache  sa belle  robe du soir
Alors  les réverbères
Les  misérables astres des pauvres  chiens errants
s'éteignent  brusquement
Et c'est encore une fois  le  viol  de  la nuit
Les étoiles filantes tombant  sur  le  trottoir
s'éteignant   à  leur  tour
et dans  les lambeaux  du  satin   sanglant  et  noir
surgit le petit  jour
le petit jour  mort-né fébrile et  blême
et qui  promène  éperdument
son   petit  corps de  revenant
empêtré dans son linceul  gris
dans  le placenta de la  nuit
Alors arrive son  grand frère
le  Grand jour
qui le  balance   à  la Seine

Quelle  famille

Et avec  ça le père  dénaturé
Le père  soleil  indifférent
qui
sans se  soucier  le moins du  monde
des avatars de ses enfants
se mire  complaisamment  dans les glaces
du  métro   aérien
qui   traverse  le pont  d' Austerlitz
comme  chaque  matin
emportant  approximativement
le  même   nombre  de  créatures humaines
de la  rive  droite  à  la rive  gauche
et de la rive  gauche   à la rive  droite
de  la Seine
Il a tant  de choses  à  faire  le soleil
et certaines de  ces choses
tout  de même  lui  font  beaucoup  de peine
par exemple
réveiller  la  lionne  du  Jardin  des  Plantes
quelle sale  besogne
et comme  il  est désespéré et  beau
et  déchirant
inoubliable
le  regard  qu'elle  a  en  découvrant comme  chaque matin
à son  réveil
les  épouvantables  barreaux de  l'épouvantable   bêtise humaine
les barreaux de  sa cage oubliée dans son  sommeil
Et le  soleil  traverse  à  nouveau  la Seine
sur un  pont   dont  il  ne sera  pas question  ici
à cause  d'une  invraisemblable  statue  de   sainte   Geneviève
veillant sur  Paris
Et  le  soleil   se promène  dans  l'île Saint  Louis
et  il  a beaucoup  de  belles et  tendres choses
à dire  sur elle
mais ce sont  des choses  secrètes entre  l’île et lui
Et le voilà  dans  Quatrième
ça c'est un coin qu'il  aime
un quartier  qu'il  a  à  la bonne
et comme  il  était  triste le  soleil
quand l'étoile  jaune de la cruelle  connerie  humaine
jetait son  ombre  parait-il  inhumaine
sur la  plus belle  rose  de  la rue  des  rosiers
Elle  s'appelait   Sarah
ou Rachel
et son  père  était  casquettier
ou fourreur
et il aimait  beaucoup les harengs  salés
Et  tout  ce qu'on sait  d'elle
c'est que le  roi  de  Sicile  l'aimait
Quand  il  sifflait  dans   ses doigts
la fenêtre  s’ouvrait là  où elle  habitait
mais jamais  plus  elle  n'ouvrira  la fenêtre
la porte d'un wagon  plombé
une fois  pour  toutes  s'est  refermée sur  elle
Et le  soleil  vainement essaye  d'oublier  ces  choses
et il poursuit  sa route
à nouveau  attiré  par  la Seine
Mais  il  s'arrête un  instant  rue  de  Jouy
pour  briller  un  peu
Tout  près de la  rue   François-Miron
là  où il  y a  une  très sordide  boutique
de vêtements d'occasion
et puis  un  coiffeur et  un  restaurant   algérien
et  puis  en  face
des ruines des  plâtras des démolitions
Et  le  coiffeur  sur le  pas de la  porte
contemple  avec stupeur
ce paysage  ébréché
et il  jette  un coup  d’œil  désespéré
vers  la  rue   Geoffroy-l'Asnier
qui  apparait   maintenant  dans  le  soleil
intacte  et  neuve
avec ses  maisons  des siècles  passés
parce que le  soleil
il y a de  cela des siècles
était  au mieux  avec  Geoffroy-l-Asnier
Tu es  un ami  lui  disait-il
et jamais  je ne  te  laisserai  tomber
Et  c'est pourquoi
l'ombre  heureuse  et  ensoleillée
l'ombre  de  Geoffroy-l'Asnier
qui  aimait  le  soleil
 et que  le soleil  aimait
s'en  va chaque  jour
que ce soit  l'hiver  ou  l'été
par la rue  du  Grenier -sur-l'eau
et par la rue  des Barres
jusqu'à  la Seine
et là  les  ombres de ses  tendres animaux
broutent les  doux chardons  de l'au-delà
et boivent l'eau  paisible
du  souvenir   heureux
