# Paris historique

https://www.paris-historique.org/

> Préserver le patrimoine bâti afin qu’il continue à s’intégrer harmonieusement dans la ville d’aujourd’hui et de demain, tel est le défi que veulent relever l’association, ses bénévoles et ses adhérents. Issue de la dynamique du Festival du Marais, Paris historique est une association loi 1901, d’intérêt général. Née en 1963, elle a pour mission de promouvoir et de protéger les quartiers de Paris et des villes de la région Île-de-France.
