# Paris mai

Paroles avec logo

Paroles de chansons / N/ Claude Nougaro/ Paris Mai  
Corriger les paroles  
Paroles de Claude NOUGARO  
Musique de Eddy LOUISS  
© EMI MUSIC PUBLISHING FRANCE, LES EDITIONS DU CHIFFRE NEUF  
Claude Nougaro - Paris Mai Lyrics & Traduction

Mai mai mai Paris mai  
Mai mai mai Paris

Le casque des pavés ne bouge plus d'un cil  
La Seine de nouveau ruisselle d'eau bénite

Le vent a dispersé les cendres de Bendit  
Et chacun est rentré chez son automobile.  
J'ai retrouvé mon pas sur le glabre bitume  
Mon pas d'oiseau forçat enchaîné à sa plume  
Et piochant l'évasion d'un rossignol titan  
Capable d'assurer le Sacre du Printemps.  
Ces temps ci, je l'avoue, j'ai la gorge un peu âcre  
Le Sacre du Printemps sonne comme un massacre  
Mais chaque jour qui vient embellira mon cri  
Il se peut que je couve un Igor Stravinski

Mai mai mai Paris mai  
Mai mai mai Paris

Et je te prends Paris dans mes bras pleins de zèle  
Sur ma poitrine je presse tes pierreries

Je dépose l'aurore sur tes Tuileries  
Comme rose sur le lit d'une demoiselle.  
Je survole à midi tes six millions de types  
Ta vie à ras le bol me file au ras des tripes  
J'avale tes quartiers aux couleurs de pigeon,  
Intelligence blanche et grise religion  
Je repère en passant Hugo dans la Sorbonne  
Et l'odeur d'eau de vie de la vieille bonbonne  
Aux lisières du soir, mi manne, mi mendiant,  
Je plonge vers un pont où penche un étudiant

Mai mai mai Paris mai  
Mai mai mai Paris

Le jeune homme harassé déchirait ses cheveux  
Le jeune homme hérissé arrachait sa chemise :  
" Camarade, ma peau est elle encore de mise  
Et dedans mon cœur seul ne fait il pas vieux jeu ?