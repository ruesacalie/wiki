# Un liieu d'écriture des _Champs magnétiques_

## Inscription

> 1919 - 1989\
Dans cet hôtel\
au cours du printemps 1919\
André BRETON & Philippe SOUPAULT\
ont inventé l'écriture automatique\
et donné naissance au surréalisme\
en écrivant "Les champs magnétiques"

----

L'écriture automatique avait déjà été envisagée par Hippolyte Taine :

> L'écriture automatique était une pratique spirite déjà rapportée par Hippolyte Taine dans la préface de la troisième édition de son ouvrage _De l'intelligence_ paru en 1878 : "Il y a une personne qui, en causant, en chantant, écrit sans regarder son papier des phrases suivies et même des pages entières, sans avoir conscience de ce qu'elle écrit. À mes yeux, sa sincérité est parfaite ; or, elle déclare qu'au bout de sa page, elle n'a aucune idée de ce qu'elle a tracé sur le papier. Quand elle le lit, elle en est étonnée, parfois alarmée... Certainement on constate ici un dédoublement du moi, la présence simultanée de deux séries d'idées parallèles et indépendantes, de deux centres d'actions, ou, si l'on veut, de deux personnes morales juxtaposées dans le même cerveau ; chacune a une œuvre, et une œuvre différente, l'une sur la scène et l'autre dans la coulisse.".  – [Wikipédia](https://fr.wikipedia.org/wiki/Écriture_automatique)
