# Rue des maléfices - Jacques Yonnet


> Raymond Queneau regardait Rue des Maléfices comme le plus grand livre jamais écrit sur Paris ; un livre qui l'empêchait même de dormir tant les histoires "vraies" que Jacques Yonnet raconte ne sont pas de tout repos. Fin connaisseur des venelles sombres et des garnis de la rive gauche, ce dernier parle du quotidien des artisans, voyous et gouailleuses de cette vieille capitale qui est "comme une mare, avec ses couleurs, ses reflets, ses fraîcheurs et sa bourbe, ses bouillonnements, ses maléfices, sa vie latente". A la manière d'un Cendrars, Jacques Yonnet évoque, par les faits divers et les drames, le quotidien de ce monde aujourd'hui disparu : Paris mystérieux où la Bièvre se jetait encore à ciel ouvert dans les marais de la Seine...

> Né en juin 1915 et mort en 1974, Jacques Yonnet fut poète, parolier, dessinateur, peintre, journaliste, sculpteur et résistant. Rue des Maléfices, salué par Jacques Prévert, est considéré comme l'un des meilleurs livres écrits sur le Paris de l'Occupation.

https://www.eyrolles.com/Arts-Loisirs/Livre/rue-des-malefices-9782752909473/
