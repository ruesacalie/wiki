# Il est cinq heures — Jacques Le Glou

Les 403 sont renversées,  
La grève sauvage est générale,  
Les Fords finissent de brûler,  
Les Enragés ouvrent le bal.  
Il est cinq heures, Paris s’éveille. (bis)  
Les blousons noirs sont à l’affût,  
Lance-pierres contre lacrymogènes,  
Les flics tombent morts au coin des rues,  
Nos petites filles deviennent des reines.  
La tour Eiffel a chaud aux pieds,  
L’Arc de Triomphe est renversé,  
La place Vendôme n’est que fumée,  
Le Panthéon s’est dissipé.  
Les maquisards sont dans les gares,  
À Notre-Dame on tranche le lard,  
Paris retrouve ses fêtards,  
Ses flambeurs et ses communards.  
Toutes les Centrales sont investies,  
Les bureaucrates exterminés,  
Les flics sont sans merci pendus  
À la tripaille des curés.  
Le vieux monde va disparaître,  
Après Paris, le monde entier.  
Les ouvriers, sans dieu, sans maître,  
Autogestionnent la cité.  
Il est cinq heures,  
Le nouveau monde s’éveille.  
Il est cinq heures,  
Et nous n’aurons jamais sommeil.

Source : [France 3 régions Paris Île-de-France](https://france3-regions.francetvinfo.fr/paris-ile-de-france/paris/il-est-cinq-heures-paris-s-eveille-ce-que-vous-ignorez-peut-etre-encore-chanson-jacques-dutronc-1590403.html)