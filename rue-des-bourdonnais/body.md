# Rue des Bourdonnais

> Cette petite section de l’ancienne rue Thibautodé portait le nom d’Arche Marion pour deux raisons : il y avait une arche de pierre qui faisait porte du côté du quai de la Mégisserie, et une dénommée Marion y tenait au milieu du XVIe siècle un établissement d’étuves (des bains publics). La rue s’appelait avant la ruelle des Étuves aux femmes (vers 1530). – Source : [vergue.com](http://vergue.com/post/162/Rue-de-l-Arche-Marion)
