# Plaque Vercors, pont des Arts


> Ce lieu du monde, unique et prestigieux, qui hantait ses pensées, nourrissait ses rêves, exaltait son âme : le pont des Arts. – Vercors, _La Marche à l'Étoile_
