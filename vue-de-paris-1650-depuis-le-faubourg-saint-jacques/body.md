# Vue de Paris, vers 1650, depuis le faubourg Saint-Jacques

- Source : Le Paris d'autrefois, éditions Skira
- Légende : Anonyme - Vue générale de Paris prise du faubourg Saint-Jacques, fragment, vers 1650. Paris, Musée Carnavalet
