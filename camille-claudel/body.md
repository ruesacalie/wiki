# Camille Claudel

> De 1879 à 1881, les Claudel habitent à Wassy (Haute-Marne). Camille Claudel persuade sa famille d'emménager à Paris, à l'exception de son père retenu par ses obligations professionnelles, afin de perfectionner son art auprès des maîtres. Les trois enfants et leur mère habitent au no 135 bis boulevard du Montparnasse, de 1882 à 1886. Source : Wikipédia

> En 1882, elle loue un atelier au no 117 rue Notre-Dame-des-Champs, où d'autres sculptrices viennent la rejoindre, la plupart anglaises, dont Jessie Lipscomb avec qui elle se lie d'une profonde amitié. Source : <http://www.musee-rodin.fr/fr/ressources/fiches-educatives/rencontre-rodin-et-camille-claudel>

> Elle habite occasionnellement au no 31 boulevard de Port-Royal de 1886 à 1892. Source : Album Claudel par Guy Goffette, bibliothèque de la Pléiade, Éditions Gallimard, 2011, p. 40 et 53-54 (ISBN 978-2-07-012375-9) via Wikipédia

> L'hôtel de Jassaud, no 19 quai de Bourbon à Paris, où Camille Claudel habita et travailla de 1899 jusqu'à son internement.