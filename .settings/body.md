# Settings

title: Rue Sacalie
locales: ["fr"]
logo: logo.svg@.settings
style: style.css@.settings
home: accueil
show-tab-one: false
necklace: true
cerbero:
  stromae: boljo
  view: ongaziwas
  signup: true 
tabs:
  max: 1
