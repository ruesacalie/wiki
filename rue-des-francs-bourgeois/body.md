# Rue des Francs-Bourgeois

## Extrait du _[Guide de Paris mystérieux](guide-de-paris-mysterieux)_

> ### De francs coquins
>
> En 1415, un nommé Le Mazurier fit don au grand prieur de France d'une vaste maison de vingt-quatre chambres, rue des Francs-Bourgeois, à charge d'y loger quarante-huit pauvres. Et, « parce que les misérables qu'on y retiroit, étoient exempts, ou francs, de payer ni boues, ni pauvres, ni lanternes, à quoi sont sujets les bourgeois de Paris, on les appela Francs-Bourgeois, et on donna à leur rue le nom de Francs-Bourgeois, au lieu de celui de la rue des Poulies qu'elle prenoit auparavant \[1\] ».
>
>Le Mazurier regretta peut-être son geste car « tandis qu'ils demeurèrent ils y firent tous les désordres que font d'ordinaire les mauvais pauvres ; la nuit, ils étourdissoient les voisins par leur tintamarre ; le soir, ils pilloient et voloient tout ce qui se rencontroit en leur quartier ; un un mot, à tout heure leur rue et leur maison étoit un coupe-gorge, et un asyle de débauche et de prostitution \[2\] ». P. D.
>
> 1. et 2. H. Sauval : _Histoire et recherches des antiquités de la ville de Paris_, 1724
