# Il est cinq heures, Paris s'éveille

<https://fr.m.wikipedia.org/wiki/Il_est_cinq_heures>,_Paris_s%27%C3%A9veille

<https://france3-regions.francetvinfo.fr/paris-ile-de-france/paris/il-est-cinq-heures-paris-s-eveille-ce-que-vous-ignorez-peut-etre-encore-chanson-jacques-dutronc-1590403.html>

Je suis le dauphin de la place Dauphine  
Et la place Blanche a mauvaise mine  
Les camions sont pleins de lait  
Les balayeurs sont pleins de balais

Il est cinq heures  
Paris s'éveille  
Paris s'éveille

Les travestis vont se raser  
Les stripteaseuses sont rhabillées  
Les traversins sont écrasés  
Les amoureux sont fatigués

Il est cinq heures  
Paris s'éveille  
Paris s'éveille

Le café est dans les tasses  
Les cafés nettoient leurs glaces  
Et sur le boulevard Montparnasse  
La gare n'est plus qu'une carcasse

Il est cinq heures  
Paris s'éveille  
Paris s'éveille

Les banlieusards sont dans les gares  
A la Villette on tranche le lard  
Paris by night, regagne les cars  
Les boulangers font des bâtards

Il est cinq heures  
Paris s'éveille  
Paris s'éveille

La tour Eiffel a froid aux pieds  
L'Arc de Triomphe est ranimé  
Et l'Obélisque est bien dressé  
Entre la nuit et la journée

Il est cinq heures  
Paris s'éveille  
Paris s'éveille

Les journaux sont imprimés  
Les ouvriers sont déprimés  
Les gens se lèvent, ils sont brimés  
C'est l'heure où je vais me coucher

Il est cinq heures  
Paris se lève  
Il est cinq heures  
Je n'ai pas sommeil