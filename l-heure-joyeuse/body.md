# L'Heure joyeuse

- https://www.paris.fr/equipements/bibliotheque-l-heure-joyeuse-2882
- « La bibliothèque L'Heure joyeuse a été inaugurée le 12 novembre 1924, au 3 rue Boutebrie près de la Sorbonne, entre le musée de Cluny et l'église Saint-Séverin. Elle fut la première bibliothèque municipale de France créée spécialement pour la jeunesse. »  Source : Wikipédia
