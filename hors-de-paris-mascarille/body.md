# Pour moi, je tiens que hors de Paris, il n’y a point de salut pour les honnêtes gens.  — Marquis de Mascarille

Cathos

Mais de grâce, Monsieur, ne soyez pas inexorable à ce fauteuil qui vous tend les bras il y a un quart d’heure ; contentez un peu l’envie qu’il a de vous embrasser.

Mascarille, après s’être peigné et avoir ajusté ses canons.

Eh bien, Mesdames, que dites-vous de Paris ?

Magdelon

Hélas ! qu’en pourrions-nous dire ? Il faudroit être l’antipode de la raison, pour ne pas confesser que Paris est le grand bureau des merveilles, le centre du bon goût, du bel esprit et de la galanterie.

Mascarille

Pour moi, je tiens que hors de Paris, il n’y a point de salut pour les honnêtes gens.

Cathos

C’est une vérité incontestable.

Mascarille

Il y fait un peu crotté ; mais nous avons la chaise \[1\].

\[1\] La chaise a porteurs dont la mode avoit été apportée d'Angleterre sous le règne de Louis XIII, par le marquis de Montbrun.

Source : https://fr.m.wikisource.org/wiki/Les_Pr%C3%A9cieuses_ridicules/%C3%89dition_Louandre,_1910





